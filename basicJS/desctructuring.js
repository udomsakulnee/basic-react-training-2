const arr = [1, 2, 3];
const [a, b, c] = arr;

const person = {
    age: 29,
    gender: "male",
    name: {
        first: "Wisut",
        last: "Udomsakulnee",
    }
}
const {gender, age, name: {first, nickname = ""}} = person;

console.log(nickname);