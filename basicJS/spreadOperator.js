const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];
const arr3 = [...arr2, 9, 10, ...arr1];

const obj1 = {a: 1, b: 2, c: 5};
const obj2 = {c: 3, d: 4};
const obj3 = {...obj2, ...obj1};

// {c: 5, d: 4, a: 1, b: 2}

console.log(obj3);