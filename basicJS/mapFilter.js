// function map = for
const arr = [1, 2, 3, 4]; // => [2, 4, 6, 8]
const newArr = arr.map(item => item * 2);

const newArr2 = arr.filter(item => item % 2 === 0);

console.log(newArr2);