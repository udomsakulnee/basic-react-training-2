import styled from "styled-components";

export const Container = styled.div`
  background-image: linear-gradient(#fc6c48, #ef5081);
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;