import styled from "styled-components";
import {colors} from "../../themes/styles";

export const Wrapper = styled.div`
  width: ${({width}) => width ? width : '300px'};
  background-color: #ffffff;
  border-radius: 10px;
  border: 1px solid ${colors.gray};
  padding: 20px;
`;
