import React, { useState } from "react";
import { Input, Button } from "reactstrap";
import styles from "./InputAdd.module.css";
import PropTypes from "prop-types";

export const InputAdd = ({ onChange }) => {
  const [text, setText] = useState("");

  const addText = () => {
    if (text !== "") {
      onChange(text);
      setText("");
    }
  };

  return (
    <div className={styles.container}>
      <Input
        className={styles.input}
        type="text"
        value={text}
        onChange={(e) => setText(e.target.value)}
      />
      <Button type="button" color="primary" onClick={addText}>
        +
      </Button>
    </div>
  );
};

InputAdd.defaultProps = {
  onChange: () => {},
};

InputAdd.propTypes = {
  onChange: PropTypes.func,
};
