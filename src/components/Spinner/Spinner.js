import React from "react";
import { Loader } from "./Spinner.style";
import { useSelector } from "react-redux";

export const Spinner = () => {
  const { isShow } = useSelector((state) => state.spinner);

  return (
    isShow && <Loader>
      <div class="loader">Loading...</div>
    </Loader>
  );
};
