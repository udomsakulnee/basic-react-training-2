import React from "react";
import PropTypes from "prop-types";
import styles from "./Item.module.css";
import { Button } from "reactstrap";

export const Item = ({ children, onDelete }) => {
  return (
    <div className={styles.container}>
      {children}
      <Button type="button" color="danger" size="sm" onClick={onDelete}>
        <i class="fas fa-trash-alt"></i>
      </Button>
    </div>
  );
};

Item.defaultProps = {
  children: "",
  onDelete: () => {},
};

Item.propTypes = {
  children: PropTypes.string,
  onDelete: PropTypes.func,
};
