import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Item } from "../../components/shared";
import styles from "./List.module.css";

export const List = ({ value, onDelete }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    setData(value);
  }, [value]);

  return (
    <div>
      <p>You have {data.length} pending items</p>
      <div className={styles.list}>
        {data.map((item, index) => (
          <Item key={index} onDelete={() => onDelete(index)}>{item}</Item>
        ))}
      </div>
    </div>
  );
};

List.defaultProps = {
  value: [],
  onDelete: () => {},
};

List.propTypes = {
  value: PropTypes.array,
  onDelete: PropTypes.func,
};
