export const colors = {
  primary: "#0066cc",
  gray: "#cccccc",
};

export const fonts = {
  small: "12px",
  normal: "16px",
  large: "20px",
};
