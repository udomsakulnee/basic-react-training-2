import React from "react";
import "./App.css";

const App = () => {
  return (
    <div>
      <p className="name1">name 1</p>
      <p className="name1 name2">name 1 name 2</p>
      <p className="name3">name 3</p>

      <div className="parent">
        <p className="child">child 1</p>
        <div>
          <p className="child">child 2</p>
        </div>
      </div>
      <p className="child">child 3</p>

      <div className="box-model">BOX Model</div>

      <h1>Header 1</h1>
      <p>Paragraph</p>
      <span className="span">SPAN</span>
      <span className="span">SPAN</span>
      <span className="span">SPAN</span>
      <span className="span">SPAN</span>

      <br />
      <div style={{position: 'relative'}}>
        <div className="box">box 1</div>
        <div className="box box2">box 2</div>
        <div className="box">box 3</div>
        <div className="box">box 4</div>
      </div>
    </div>
  );
};

export default App;
