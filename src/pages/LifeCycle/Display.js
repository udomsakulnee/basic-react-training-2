import React, { useEffect } from "react";

const Display = ({ firstname, lastname }) => {
  useEffect(() => {
    console.log("mounting");
  }, []);

  useEffect(() => {
    return () => {
      console.log("unmount");
    };
  });

  return (
    <p>
      {firstname} {lastname}
    </p>
  );
};

export default Display;
