import React, {useState, useEffect} from "react";
import Display from "./Display";

const App = () => {
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [toggle, setToggle] = useState(false);

  useEffect(() => {
    console.log(firstname);
  }, [firstname]);

  useEffect(() => {
    console.log(lastname);
  }, [lastname]);

  return (
    <div>
      <div>
        <label>firstname: </label>
        <input type="text" value={firstname} onChange={(e) => setFirstname(e.target.value)} />
      </div>

      <div>
        <label>lastname: </label>
        <input type="text" value={lastname} onChange={(e) => setLastname(e.target.value)} />
      </div>

      <br />
      <button type="button" onClick={() => setToggle(!toggle)}>toggle</button>
      {toggle && <Display firstname={firstname} lastname={lastname} />}
    </div>
  )
}

export default App;