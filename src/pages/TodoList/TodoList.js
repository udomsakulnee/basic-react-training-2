import React, { useState } from "react";
import styles from "./TodoList.module.css";
import { Container, Wrapper, InputAdd, List } from "../../components/shared";

const TodoList = () => {
  const [todoList, setTodoList] = useState([]);

  const addTodo = (value) => {
    const list = [...todoList, value];
    setTodoList(list);
  };

  const handleDelete = (key) => {
    const filterTodoList = todoList.filter((item, index) => key !== index);
    setTodoList(filterTodoList);
  };

  return (
    <Container>
      <Wrapper>
        <h4 className={styles.title}>TodoList</h4>
        <InputAdd onChange={addTodo} />
        <List value={todoList} onDelete={handleDelete} />
      </Wrapper>
    </Container>
  );
};

export default TodoList;
