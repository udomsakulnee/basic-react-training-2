import React from "react";

const App = () => {
  const lists = [
    {
      name: {
        first: "Alexandre",
        last: "Sanchez",
      },
      gender: "male",
      age: 31,
    },
    {
      name: {
        first: "Layla",
        last: "Anderson",
      },
      gender: "female",
      age: 42,
    },
    {
      name: {
        first: "Elisabeth",
        last: "Stensrud",
      },
      gender: "female",
      age: 43,
    },
  ];

  const filterLists = lists.filter((item) => item.gender === "female");
  
  return (
    <div>
      <h2>List Example</h2>
      {
        filterLists.map(({name: {first, last}, gender, age}, index) => {
          return (
            <div key={index}>
              <h2>{`${first} ${last}`}</h2>
              <p>Gender: {gender} | Age: {age}</p>
            </div>
          )
        })
      }
    </div>
  )
}

export default App