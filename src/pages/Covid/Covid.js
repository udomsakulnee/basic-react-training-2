import React, { useEffect, useState } from "react";
import { Container, Wrapper } from "../../components/shared";
import BoxCovid from "./BoxCovid";
import { CovidContainer } from "./Covid.style";
import api from "../../api/api";

const Covid = () => {
  const [data, setData] = useState({});

  const fetchData = async () => {
    try {
      const { status, data } = await api.get("/api/open/today");
      if (status === 200) {
        setData(data);
      }
    } catch (err) {}
  };

  useEffect(() => {
    fetchData();
  }, []);

  const {
    UpdateDate,
    Confirmed,
    NewConfirmed,
    Recovered,
    NewRecovered,
    Hospitalized,
    NewHospitalized,
    Deaths,
    NewDeaths,
  } = data;

  return (
    <Container>
      <Wrapper width="640px">
        <h1>รายงานสถานการณ์โควิด-19</h1>
        <h2>อัปเดตข้อมูลล่าสุด : {UpdateDate}</h2>
        <div>
          <BoxCovid
            color="#e1298e"
            title="ติดเชื้อสะสม"
            total={Confirmed}
            increase={NewConfirmed}
          />
        </div>
        <CovidContainer>
          <BoxCovid
            color="#046034"
            title="หายแล้ว"
            total={Recovered}
            increase={NewRecovered}
          />
          <BoxCovid
            color="#179c9b"
            title="รักษาตัวอยู่ใน รพ."
            total={Hospitalized}
            increase={NewHospitalized}
          />
          <BoxCovid
            color="#666666"
            title="เสียชีวิต"
            total={Deaths}
            increase={NewDeaths}
          />
        </CovidContainer>
      </Wrapper>
    </Container>
  );
};

export default Covid;
