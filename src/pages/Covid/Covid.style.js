import styled from "styled-components";

export const Container = styled.div`
  background-color: ${({ color }) => color};
  padding: 10px;
  margin: 10px;
  border-radius: 10px;
  flex: 1;

  h5,
  h1,
  p {
    text-align: center;
    margin: 0px;
    color: #ffffff;
  }

  h1 {
    font-size: 40px;
  }
`;

export const CovidContainer = styled.div`
  display: flex;
`;
