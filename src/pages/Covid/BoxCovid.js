import React from "react";
import { Container } from "./Covid.style";
import PropTypes from "prop-types";

const BoxCovid = ({ color, title, total, increase }) => {
  return (
    <Container color={color}>
      <h5>{title}</h5>
      <h1>{total}</h1>
      <p>(+{increase})</p>
    </Container>
  );
};

export default BoxCovid;

BoxCovid.defaultProps = {
  color: "#ffffff",
  title: "",
  total: 0,
  increase: 0,
};

BoxCovid.propTypes = {
  color: PropTypes.string,
  title: PropTypes.string,
  total: PropTypes.number,
  increase: PropTypes.number,
};
