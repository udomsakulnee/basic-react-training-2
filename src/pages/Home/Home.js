import React from "react";
import { useHistory } from "react-router-dom";
import {Container} from "../../components/shared";
import {Button} from "reactstrap";

const Home = () => {
  const history = useHistory();

  const gotoPage = (page) => {
    history.push(page);
  }

  return (
      <Container>
          <Button type="button" onClick={() => gotoPage("/todolist")}>TodoList</Button>
          <Button type="button" onClick={() => gotoPage("/covid")}>Covid</Button>
          <Button type="button" onClick={() => gotoPage("/user")}>User</Button>
      </Container>
  );
};

export default Home;
