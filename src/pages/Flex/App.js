import React from "react";
import "./App.css";

const App = () => {
  return (
    <div className="parent">
      <div className="child">CHILD 1</div>
      <div className="child">CHILD 2</div>
      <div className="child">CHILD 3</div>
      <div className="child">CHILD 4</div>
    </div>
  );
};

export default App;
