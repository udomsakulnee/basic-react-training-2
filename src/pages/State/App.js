import React, {useState} from "react";
import "./App.css";

const App = () => {
  // [0, 0, 0]
  const [digit, setDigit] = useState([0, 0, 0]);

  const randomNumber = () => {
    return Math.ceil(Math.random() * 9);
  }

  const random = () => {
    setDigit([randomNumber(), randomNumber(), randomNumber()]);
  }

  return (
    <div className="container">
      <h2>Random Number</h2>
      <div>
        {/* expression */}
        <span>{digit[0]}</span>
        <span>{digit[1]}</span>
        <span>{digit[2]}</span>
      </div>
      <button type="button" onClick={random}>Random</button>
    </div>
  );
};

export default App;
