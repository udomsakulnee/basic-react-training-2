import React, {useState} from "react";

const App = () => {
  const [money, setMoney] = useState(0);
  const [result, setResult] = useState(0);

  // calculateVat
  const calculateVat = () => {
    const moneyIncludeVat = money * 1.07;
    setResult(moneyIncludeVat);
  }

  return (
    <div>
      <h2>Vat Calculator</h2>
      <p>{money}</p>
      <div>
        <label>Money: </label>
        <input type="text" value={money} onChange={(e) => setMoney(e.target.value)} />
        <button type="button" onClick={calculateVat}>Calculate</button>
      </div>
      <p>Result: {result}</p>
    </div>
  )
}

export default App