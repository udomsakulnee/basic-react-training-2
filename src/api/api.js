import axios from "axios";

// const baseURL = "https://covid19.th-stat.com";
const baseURL = process.env.REACT_APP_API_URL;

// GET ดึงข้อมูล readonly
// POST สร้างข้อมูลใหม่ create
// PUT อัปเดตข้่อมูล update
// DELETE ลบข้อมูล delete

const defaultOptions = {
  baseURL,
  method: "GET",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json; charset=utf-8",
  },
};

const api = axios.create(defaultOptions);
export default api;
