import { useState, useEffect } from "react";
import api2 from "../api/api2";
import { useDispatch } from "react-redux";
import { showSpinner, hideSpinner } from "../redux/actions";

const useFetch = (url) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(true);
  const [data, setData] = useState({});
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      dispatch(showSpinner());
      try {
        const response = await api2.get(url);
        if (response.status === 200) {
          setIsSuccess(true);
          setData(response.data);
        } else {
          setIsSuccess(false);
        }

        setIsLoading(false);
        dispatch(hideSpinner());
      } catch (err) {
        setIsLoading(false);
        setIsSuccess(false);
        dispatch(hideSpinner());
      }
    };

    fetchData();
  }, [url, dispatch]);

  return {
    data,
    isLoading,
    isSuccess,
  };
};

export default useFetch;
