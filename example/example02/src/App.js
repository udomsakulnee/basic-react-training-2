import React, {useState} from 'react';
import './App.css';

const App = () => {
  const [money, setMoney] = useState();
  const [result, setResult] = useState(0);

  const handleChange = (e) => {
    setMoney(e.target.value);
  }

  const calculate = () => {
    const res = Number(money) * 1.07;
    setResult(res);
  }

  return (
    <div className="container">
      <h1>Vat Calculator</h1>
      <label>Money : </label>
      <input type="text" value={money} onChange={handleChange} />
      <button type="button" onClick={calculate}>Calculate</button>
      <p>Result : {result}</p>
    </div>
  );
}

export default App;
