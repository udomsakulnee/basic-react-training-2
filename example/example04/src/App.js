import React, {useState, useEffect} from 'react';
import './App.css';

const Display = ({firstname, lastname}) => {
  useEffect(() => {
    console.log('inital');
  }, []);

  useEffect(() => {
    return () => {
      console.log('unmount');
    }
  })

  return <p>{`${firstname} ${lastname}`}</p>
}

const App = () => {
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [toggle, setToggle] = useState(false);

  useEffect(() => {
    console.log(firstname);
  }, [firstname]);

  return (
    <div className="container">
      <div>
        <label>firstname : </label>
        <input type="text" value={firstname} onChange={(e) => setFirstname(e.target.value)} />
      </div>
      <br />
      <div>
        <label>lastname : </label>
        <input type="text" value={lastname} onChange={(e) => setLastname(e.target.value)} />
      </div>
      
      <br />
      <button type="button" onClick={() => setToggle(!toggle)}>toggle</button>
      {toggle && <Display firstname={firstname} lastname={lastname} />}
    </div>
  );
};

export default App;
