import React from "react";

const Test = ({ onClick }) => {

  const handleOnClick = () => {
    onClick();
  }

  return (
    <div>
      <p>TEST ERROR</p>
      <button onClick={handleOnClick}>TEST</button>
    </div>
  );
};

export default Test;
