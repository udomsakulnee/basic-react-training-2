import React from "react";
import { Loader } from "./Spinner.style";
import { useSelector } from "react-redux";

const Spinner = () => {
  const { isShow } = useSelector((state) => state.spinner);

  return (
    isShow && <Loader>
      <div class="loader">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </Loader>
  );
};

export default Spinner;
