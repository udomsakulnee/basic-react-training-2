import React from "react";
import { Container, Table, Button } from "reactstrap";
import { get } from "lodash";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import useFetch from "../../hooks/useFetch";
import { showPopup } from "../../redux/actions";
import Test from "../../components/Test/Test";

const Home = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { data, isLoading, isSuccess } = useFetch("/?results=10");
  let userData = get(data, "results", []);

  const handleClick = (params) => {
    dispatch(
      showPopup({
        title: `${get(params, "user.name.first", "")} ${get(
          params,
          "user.name.last",
          ""
        )}`,
        description: "do you want to open user profile detail?",
        action: () => {
          history.push("/user", params);
        },
      })
    );
  };

  return (
    <Container>
      {/* <Test /> */}
      {isLoading ? (
        <p>loading...</p>
      ) : (
        <Table hover striped responsive>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Age</th>
              <th>Gender</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {userData.map((item, index) => (
              <tr key={get(item, "login.uuid", index)}>
                <th scope="row">{index + 1}</th>
                <td>
                  <img
                    className="rounded-circle mr-3"
                    alt={get(item, "id.name", "")}
                    src={get(item, "picture.thumbnail", "")}
                  />
                  {`${get(item, "name.first", "")} ${get(
                    item,
                    "name.last",
                    ""
                  )}`}
                </td>
                <td>{get(item, "dob.age", "")}</td>
                <td>
                  {get(item, "gender", "") === "male" ? (
                    <i className="fas fa-mars text-info"></i>
                  ) : (
                    <i className="fas fa-venus text-danger"></i>
                  )}
                </td>
                <td>{get(item, "email", "")}</td>
                <td>{get(item, "phone", "")}</td>
                <td>
                  <Button
                    color="primary"
                    size="sm"
                    onClick={() => handleClick({ user: item })}
                  >
                    View
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      )}
    </Container>
  );
};

export default Home;
