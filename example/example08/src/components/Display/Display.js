import React from "react";
import PropTypes from "prop-types";
import styles from "./Display.module.css";

const Display = ({ value }) => {
  return <h1 className={styles.display}>{value}</h1>;
};

Display.defaultProps = {
  value: 0,
};

Display.propTypes = {
  value: PropTypes.number,
};

export default Display;