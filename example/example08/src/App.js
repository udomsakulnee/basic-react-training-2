import React, { useState } from "react";
import Calculator from "./components/Calculator/Calculator";

const App = () => {
  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(100);

  return (
    <div style={{ backgroundColor: "#007ac2", height: "100vh" }}>
      <Calculator value={num1} onChange={setNum1} />
      <Calculator value={num2} onChange={setNum2} />
      <p className="m-3">{`${num1} + ${num2} = ${num1 + num2}`}</p>
    </div>
  );
};

export default App;
