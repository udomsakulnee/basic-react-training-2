import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 500px;
    padding: 20px;
    border: 1px solid #cccccc;
    border-radius: 10px;
    box-shadow: 0 0 5px #cccccc;
    background-color: #ffffff;
`;