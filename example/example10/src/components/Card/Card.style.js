import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex: 1;
  box-shadow: 0 0 6px 0 #e1e5eb;
  border-radius: 8px;
  background-color: ${({ backgroundColor }) =>
    backgroundColor ? backgroundColor : "none"};
  padding: 8px;
  margin: 10px;
`;

export const Title = styled.div`
  display: flex;
  justify-content: center;
  flex: 1;
  font-size: 20px;
  color: #fff;
`;

export const Content = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  text-align: center;
  margin: 0;
  font-size: 38px;
  color: #fff;
`;

export const Content2 = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
  font-size: 18px;
  color: #fff;
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;
