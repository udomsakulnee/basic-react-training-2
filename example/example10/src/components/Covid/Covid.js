import PropTypes from "prop-types";
import Card from "../Card/Card";
import {Container, Wrapper, Column, H1, H2, Content, Row} from "./Covid.style";

const Covid = ({ data }) => {
  const {
    Confirmed = 0,
    Recovered = 0,
    Hospitalized = 0,
    Deaths = 0,
    NewConfirmed = 0,
    NewRecovered = 0,
    NewDeaths = 0,
    NewHospitalized = 0,
    UpdateDate = "",
  } = data;
  
  return (
    <Container>
      <Wrapper>
        <Column>
          <H1>รายงานสถานการณ์ โควิด-19</H1>
          <H2>อัพเดทข้อมูลล่าสุด : {UpdateDate}</H2>
          <Content>
            แนะนำการใช้แอปพลิเคชัน “หมอชนะ”
            อีกหนึ่งวิธีที่คนไทยสามารถช่วยกันหยุดการระบาดได้ ด้วยการใช้แอป
            “หมอชนะ”
            ซึ่งจะเป็นช่องทางแจ้งเตือนแนะนำผู้ที่สัมผัสความเสี่ยงให้ไปตรวจ
            กักตัว หรือปฏิบัติตัวได้เหมาะสม
            ช่วยให้การเข้าถึงการตรวจวินิจฉัยและขีดวงหยุดการระบาดมีประสิทธิภาพดีขึ้น
          </Content>
        </Column>
        <Column>
          <Card
            backgroundColor="#e1298e"
            title="ติดเชื้อสะสม"
            number={Confirmed}
            number2={`+${NewConfirmed}`}
          ></Card>
          <Row>
            <Card
              backgroundColor="#046034"
              title="หายแล้ว"
              number={Recovered}
              number2={`+${NewRecovered}`}
            ></Card>
            <Card
              backgroundColor="#179c9b"
              title="รักษาอยู่ใน รพ."
              number={Hospitalized}
              number2={`+${NewHospitalized}`}
            ></Card>
            <Card
              backgroundColor="#666"
              title="เสียชีวิต"
              number={Deaths}
              number2={`+${NewDeaths}`}
            ></Card>
          </Row>
        </Column>
      </Wrapper>
    </Container>
  );
};

Covid.defaultProps = {
  data: {},
};

Covid.propTypes = {
  data: PropTypes.object,
};

export default Covid;
