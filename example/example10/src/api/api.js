import axios from "axios";

const baseURL = "https://covid19.th-stat.com";

const defaultOptions = {
  baseURL,
  method: "GET",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json; charset=utf-8",
  },
};

const api = axios.create(defaultOptions);
export default api;
