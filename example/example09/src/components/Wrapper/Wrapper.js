import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 300px;
    padding: 20px;
    background-color: #ffffff;
    border: 1px solid #cccccc;
    border-radius: 6px;
`;