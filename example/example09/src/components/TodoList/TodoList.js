import React, { useState, useEffect } from "react";
import List from "./List";
import styles from "./TodoList.module.css";
import PropTypes from "prop-types";

export const TodoList = ({ value, onDelete }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    setData(value);
  }, [value]);

  return (
    <div>
      <p className="mt-2">You have {value.length} pending items</p>
      <div className={styles.list}>
        {data.map((item, key) => (
          <List key={key} onDelete={() => onDelete(key)}>
            {item}
          </List>
        ))}
      </div>
    </div>
  );
};

TodoList.defaultProps = {
  value: [],
  onDelete: () => {},
};

TodoList.propTypes = {
  value: PropTypes.array,
  onDelete: PropTypes.func,
};
