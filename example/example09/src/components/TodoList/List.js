import React from "react";
import styles from "./TodoList.module.css";
import PropTypes from "prop-types";
import { Button } from "reactstrap";

const List = ({children, onDelete}) => {
  return (
    <div className={styles.containerList}>
      {children}
      <Button color="danger" size="sm" className={styles.deleteButton} onClick={onDelete}>
        <i class="far fa-trash-alt"></i>
      </Button>
    </div>
  );
};

List.defaultProps = {
  children: "",
  onDelete: () => {},
};

List.propTypes = {
  children: PropTypes.string,
  onDelete: PropTypes.func,
};

export default List;
