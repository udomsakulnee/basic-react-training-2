import React, {useState} from 'react';
import { Container, Wrapper, InputAdd, TodoList } from "./components/shared";
import "./App.css";

const App = () => {
  const [todoList, setTodoList] = useState([]);

  const addTodo = (value) => {
    setTodoList([...todoList, value])
  }

  const handleDelete = (key) => {
    const filterTodoList = todoList.filter((item, index) => key !== index);
    setTodoList(filterTodoList);
  }

  return (
    <Container>
      <Wrapper>
        <h4 className="text-center m-2">Todo List</h4>
        <InputAdd onChange={addTodo} />
        <TodoList value={todoList} onDelete={handleDelete} />
      </Wrapper>
    </Container>
  );
};

export default App;
